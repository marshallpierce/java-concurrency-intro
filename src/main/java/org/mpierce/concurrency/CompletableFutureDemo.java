package org.mpierce.concurrency;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class CompletableFutureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(4);

        CompletableFuture<String> f1 = new CompletableFuture<>();
        pool.submit(() -> {
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                String foo = "foo";
                f1.complete(foo);
            } catch (Throwable e) {
                f1.completeExceptionally(e);
            }
        });

        CompletableFuture<String> f2 = new CompletableFuture<>();
        pool.submit(() -> {
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            f2.complete("bar");
        });

        f1.whenComplete((s, throwable) -> {
            if (throwable != null) {
                System.out.println("oh no");
            } else {
                System.out.println("yay");
            }
        });

        CompletableFuture<Object> f1part2 = f1.thenApply(s -> s + s);

        CompletableFuture<Object> either = CompletableFuture.anyOf(f1part2, f2);

        System.out.println("Got: " + either.get());
        System.out.println("f1: " + f1part2.get());
        System.out.println("f2: " + f2.get());
    }
}
