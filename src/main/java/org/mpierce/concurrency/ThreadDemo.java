package org.mpierce.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class ThreadDemo {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Hello world");

        AtomicInteger a = new AtomicInteger(0);

        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("I'm in a thread");

                a.set(42);
            }
        };

        t.start();
        t.join();

        System.out.println("Got: " + a.get());

        System.out.println("Main exiting");
    }

}
